const path = require('path');
const express = require('express');
const winston = require('winston');

const app = express();

const port = 3000;

winston.cli();

app.use(express.static(path.join(__dirname, 'client/public')));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/public/index.html'));
});

app.listen(port, () => winston.info(`Flowzample is listening on port ${port}.`));
