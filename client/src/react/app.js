// @flow
/* globals document, HTMLElement */

import React from 'react';
import { render } from 'react-dom';

import Page from './components/Page/Page';

const app = () => {
    const el:null|HTMLElement = document.getElementById('app-root');

    if (el) {
        render(
            <Page />,
            el,
        );
    } else {
        throw new Error('App root not presnt');
    }
};

export default app;
