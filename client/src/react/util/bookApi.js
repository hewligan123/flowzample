// @flow

import axios from 'axios';
import { find, propEq } from 'ramda';

export type bookSummary = {
    id: number,
    title: string,
    author: string
};

export type bookDetails = {
    publication: number,
    publisher: string,
    cover: string,
    summary: string
};

export type fullBookDetails = {
    id: number,
    title: string,
    author: string,
    publication: number,
    publisher: string,
    cover: string,
    summary: string
};

const bookCache: { [string]:bookDetails } = {};
let bookListCache:Array<bookSummary> = [];

export const getBookList = async ():Promise<Array<bookSummary>> => {
    if (bookListCache.length) {
        return bookListCache;
    }

    const { data }:{ data:Array<bookSummary> } = await axios.get('/data/list.json');

    bookListCache = data;
    return data;
};

export const getBook = async (id:number):Promise<bookDetails> => {
    const bookKey = `id-${id}`;

    if (bookCache[bookKey]) {
        return bookCache[bookKey];
    }

    const { data }:{ data: bookDetails } = await axios.get(`/data/${bookKey}.json`);

    bookCache[bookKey] = data;
    return data;
};

export const getFullBook = async (id:number):Promise<fullBookDetails> => {
    const [list: Array<bookSummary>, data: bookDetails] = await Promise.all([
        getBookList(),
        getBook(id),
    ]);

    const listData:?bookSummary = find(propEq('id', id), list);

    if (listData && data) {
        const fullData:fullBookDetails = Object.assign({}, listData, data);

        return fullData;
    }

    throw new Error('Book data not found');
};
