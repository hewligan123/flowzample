// @flow

import React, { Component } from 'react';
import type { RouterHistory } from 'react-router-dom';

import { getFullBook } from '../../util/bookApi';
import type { fullBookDetails } from '../../util/bookApi';

type Props = {
    history: RouterHistory,
    match: {
        isExact: boolean,
        params: {
            bookID: string
        },
        path: string,
        url: string,
    }
};
type State = {
    book: fullBookDetails,
};

class BookDetails extends Component<Props, State> {
    constructor(props:Props) {
        super(props);

        this.state = {
            book: {
                title: '',
                author: '',
                cover: '',
                id: 0,
                publication: 0,
                publisher: '',
                summary: '',
            },
        };
    }
    componentDidMount() {
        getFullBook(Number(this.props.match.params.bookID)).then((book) => {
            this.setState({ book });
        });
    }

    render() {
        return (
            <div className="book-container container">
                <div className="page-header">
                    <h2>{this.state.book.title}</h2>
                    <h2><small>{this.state.book.author}</small></h2>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <img src={`/covers/${this.state.book.cover}`} alt={`Cover for ${this.state.book.title}`} />
                    </div>
                    <div className="col-sm-8">
                        <h3>Publishing information</h3>
                        <p>Published in {this.state.book.publication} by {this.state.book.publisher}</p>
                        <h3>Summary</h3>
                        <p>{this.state.book.summary}</p>
                    </div>
                </div>

                <hr />

                <p>
                    <button className="gtn btn-primary btn-lg" onClick={() => this.props.history.goBack()}>Back</button>
                </p>
            </div>
        );
    }
}

export default BookDetails;
