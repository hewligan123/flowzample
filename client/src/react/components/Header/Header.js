// @flow

import React, { Component } from 'react';

import './Header.scss';

type props = {};

class Header extends Component<props> {
    render() {
        return (
            <header className="header-container jumbotron">
                <div className="container">
                    <h1 className="header-heading">Book List</h1>
                </div>
            </header>
        );
    }
}

export default Header;
