// @flow

import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from '../Header/Header';
import Home from '../Home/Home';
import BookList from '../BookList/BookList';
import BookDetails from '../BookDetails/BookDetails';

type Props = {};

class Page extends Component<Props> {
    render() {
        return (
            <Router>
                <div className="page-container">
                    <Header />
                    <Route exact path="/" component={Home} />
                    <Route path="/book-list" component={BookList} />
                    <Route path="/book-details/:bookID" component={BookDetails} />
                </div>
            </Router>
        );
    }
}

export default Page;
