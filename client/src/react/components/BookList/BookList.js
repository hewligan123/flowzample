// @flow

import React, { Component } from 'react';

import { getBookList } from '../../util/bookApi';
import type { bookSummary } from '../../util/bookApi';


import BookSummary from './BookSummary';

type Props = {};
type State = {
    bookList: Array<bookSummary>
};

class BookList extends Component<Props, State> {
    constructor(props:Props) {
        super(props);

        this.state = {
            bookList: [],
        };
    }

    componentDidMount() {
        getBookList().then((bookList) => {
            this.setState({ bookList });
        });
    }

    render() {
        return (
            <div className="book-list-container container">
                <div className="list-group">
                    {
                        this.state.bookList.map(book => (
                            <BookSummary book={book} key={book.id} />
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default BookList;
