// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import type { bookSummary } from '../../util/bookApi';

type Props = {
    book: bookSummary
};

class BookSummary extends Component<Props> {
    render() {
        return (
            <Link to={`/book-details/${this.props.book.id}`} className="list-group-item" key={this.props.book.id}>
                <h4 className="list-group-item-heading">{this.props.book.title}</h4>
                <p className="list-group-item-text">{this.props.book.author}</p>
            </Link>
        );
    }
}

export default BookSummary;
