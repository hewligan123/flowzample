// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

type Props = {};

class Home extends Component<Props> {
    render() {
        return (
            <div className="home-page-container container">
                <ul>
                    <li>
                        <Link to="book-list">Demo app</Link>
                    </li>
                    <li>
                        <a href="/presentation.html">Presentation</a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Home;
