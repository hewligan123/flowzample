import Reveal from 'reveal.js';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/moon.css';

import highlight from 'highlight.js';
import 'highlight.js/styles/zenburn.css';

import './presentation.scss';

Reveal.initialize({
    controls: true,
    progress: true,
    history: true,
    center: true,
    // default/cube/page/concave/zoom/linear/fade/none
    transition: 'cube',
});

highlight.initHighlightingOnLoad();
