import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Demo extends Component {
    render() {
        return (
            <p>
                {this.props.exampleText}
            </p>
        );
    }
}

Demo.propTypes = {
    exampleText: PropTypes.string.isRequired,
};

export default Demo;

