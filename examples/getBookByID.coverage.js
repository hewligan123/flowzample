// @flow
import axios from 'axios';

type bookData = {
    title:string,
    author:string
};

const getBookByID = async (id:number):Promise<bookData> => {
    const { data }:{ data:bookData } = await axios.get(`/books/${id}`);

    return data;
};

export default getBookByID;
