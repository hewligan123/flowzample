// @flow

import React, { Component } from 'react';

type Props = {
    exampleText:string
};

class Demo extends Component<Props> {
    render() {
        return (
            <p>
                {this.props.exampleText}
            </p>
        );
    }
}

export default Demo;

