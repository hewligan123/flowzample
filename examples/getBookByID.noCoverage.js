// @flow
import axios from 'axios';

const getBookByID = async (id) => {
    const { data } = await axios.get(`/books/${id}`);

    return data;
};

export default getBookByID;
